package firubingm.metropolia.users.rssfeedreader;

import java.util.ArrayList;

public class FeedStorage {
    private static FeedStorage ourInstance = new FeedStorage();

    private ArrayList<Feed> feeds;

    private FeedStorage() {
        this.feeds = new ArrayList<Feed>();
    }

    public static FeedStorage getInstance() {
        return ourInstance;
    }

    public ArrayList<Feed> getFeeds() {
        ArrayList<Feed> result = new ArrayList<Feed>();
        result.addAll(this.feeds);
        return result;
    }

    public Feed getFeed(int position) {
        return this.feeds.get(position);
    }

    public void addFeed(Feed feed) {
        this.feeds.add(feed);
    }

}
