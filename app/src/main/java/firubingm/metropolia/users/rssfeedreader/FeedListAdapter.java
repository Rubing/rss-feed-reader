package firubingm.metropolia.users.rssfeedreader;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FeedListAdapter extends ArrayAdapter<Feed> {

    private ArrayList<Feed> feeds;
    private Context context;

    public FeedListAdapter(Context context, int resource, ArrayList<Feed> objects) {
        super(context, resource, objects);
        this.feeds = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View targetView = convertView;
        if(targetView == null) {
            LayoutInflater li = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            targetView = li.inflate(R.layout.feed_layout, parent, false);
        }
        Feed feed = this.feeds.get(position);
        if(feed != null) {
            TextView title = (TextView)targetView.findViewById(R.id.feedItemTitle);
            title.setText(feed.getTitle());
            TextView description = (TextView)targetView.findViewById(R.id.feedItemDescription);
            description.setText(feed.getDescription());
        }
        return targetView;
    }
}
