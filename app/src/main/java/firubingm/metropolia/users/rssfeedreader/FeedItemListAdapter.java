package firubingm.metropolia.users.rssfeedreader;


import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FeedItemListAdapter extends ArrayAdapter<FeedItem> {

    private ArrayList<FeedItem> feedItems;
    private Context context;
    private Filter filter;

    public FeedItemListAdapter(Context context, int resource, ArrayList<FeedItem> objects) {
        super(context, resource, objects);
        this.feedItems = objects;
        this.context = context;
        this.filter = new ReadFilter(objects);
    }

    public void handleClick(int position, boolean checked) {
        FeedItem item = this.feedItems.get(position);
        synchronized (item) {
            if (checked) {
                item.markAsRead();
            } else {
                item.markAsUnread();
            }
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View targetView = convertView;
        if(targetView == null) {
            LayoutInflater li = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            targetView = li.inflate(R.layout.feed_item_layout, parent, false);
        }
        final FeedItem item = this.feedItems.get(position);
        if(item != null) {
            synchronized (item) {
                TextView title = (TextView)targetView.findViewById(R.id.feedItemTitle);
                title.setText(item.getTitle());
                TextView description = (TextView)targetView.findViewById(R.id.feedItemDescription);
                description.setText(Html.fromHtml(item.getDescription().replaceAll("<img.+/(img)*>", "")));
                TextView date = (TextView)targetView.findViewById(R.id.feedItemDate);
                date.setText(item.getDate());
                CheckBox box = (CheckBox)targetView.findViewById(R.id.checkBox);
                box.setChecked(item.getRead());
            }
        }
        return targetView;
    }

    @Override
    public Filter getFilter() {
        return this.filter;
    }

    private class ReadFilter extends Filter {

        private ArrayList<FeedItem> allItems;

        public ReadFilter(ArrayList<FeedItem> items) {
            this.allItems = new ArrayList<FeedItem>();
            this.allItems.addAll(items);
        }

        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            FilterResults result = new FilterResults();
            if (chars != null && chars.toString().equals("filter")) {
                ArrayList<FeedItem> filter = new ArrayList<FeedItem>();
                for (FeedItem item: allItems) {
                    synchronized (item) {
                        if (!item.getRead())
                            filter.add(item);
                    }
                }
                result.count = filter.size();
                result.values = filter;
            } else {
                result.values = allItems;
                result.count = allItems.size();
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<FeedItem> filtered = (ArrayList<FeedItem>) results.values;
            clear();
            addAll(filtered);
            notifyDataSetChanged();
        }
    }
}
