package firubingm.metropolia.users.rssfeedreader;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class Feed {

    private String title;
    private String description;
    private String url;
    private ArrayList<FeedItem> items;

    public Feed() {
        this.title = "";
        this.description = "";
        this.url = "";
        this.items = new ArrayList<FeedItem>();
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void addItem(FeedItem item) {
        this.items.add(item);
    }

    public ArrayList<FeedItem> getItems() {
        ArrayList<FeedItem> result = new ArrayList<FeedItem>();
        result.addAll(this.items);
        return result;
    }

    public FeedItem getItem(int position) {
        return this.items.get(position);
    }

    public String toString() {
        return "Feed: " + this.title + " - " + this.description + " (" + this.url + ")";
    }
}
