package firubingm.metropolia.users.rssfeedreader;


import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RssParser extends Thread {

    private Handler handler;
    private String url;
    private final DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    public RssParser(Handler handler, String url) {
        this.handler = handler;
        this.url = url;
    }

    public void run() {
        try {
            Feed feed = downloadFeed(this.url);
            this.sendMessage(feed);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Feed downloadFeed(String urlString) throws IOException {
        InputStream is = null;
        Feed feed = null;

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("DEBUG", "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a feed
            feed = readStream(is);

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return feed;
    }

    private Feed readStream(InputStream stream) throws IOException, XmlPullParserException {
        boolean inChannel = false;
        boolean inItem = false;
        boolean inTitle = false;
        boolean inDescription = false;
        boolean inLink = false;
        boolean inGuid = false;
        boolean inPubDate = false;
        Feed feed = new Feed();
        FeedItem item = null;
        String tag = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(stream, null);
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if(eventType == XmlPullParser.START_DOCUMENT) {
            } else if(eventType == XmlPullParser.START_TAG) {
                tag = xpp.getName();
                System.out.println(tag);
                if ("channel".equals(tag)) {
                    inChannel = true;
                } else if ("item".equals(tag)) {
                    inItem = true;
                    item = new FeedItem();
                } else if ("title".equals(tag)) {
                    inTitle = true;
                } else if ("description".equals(tag)) {
                    inDescription = true;
                } else if ("link".equals(tag)) {
                    inLink = true;
                } else if ("guid".equals(tag)) {
                    inGuid = true;
                } else if ("pubDate".equals(tag)) {
                    inPubDate = true;
                } else if ("enclosure".equals(tag)) {
                    // image found
                }
            } else if(eventType == XmlPullParser.END_TAG) {
                tag = xpp.getName();
                if ("channel".equals(tag)) {
                    inChannel = false;
                } else if ("item".equals(tag)) {
                    inItem = false;
                    feed.addItem(item);
                } else if ("title".equals(tag)) {
                    inTitle = false;
                } else if ("description".equals(tag)) {
                    inDescription = false;
                } else if ("link".equals(tag)) {
                    inLink = false;
                } else if ("guid".equals(tag)) {
                    inGuid = false;
                } else if ("pubDate".equals(tag)) {
                    inPubDate = false;
                }
            } else if(eventType == XmlPullParser.TEXT) {
                String text = xpp.getText();
                if (inChannel) {
                    if (inItem) {
                        if (inTitle) {
                            item.setTitle(text);
                        } else if (inDescription) {
                            item.setDescription(text);
                        } else if (inGuid) {
                            item.setUrl(text);
                        } else if (inPubDate) {
                            try {
                                Date date = formatter.parse(text);
                                item.setPublishDate(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        if (inTitle) {
                            feed.setTitle(text);
                        } else if (inDescription) {
                            feed.setDescription(text);
                        } else if (inLink) {
                            feed.setUrl(text);
                        }
                    }
                }
            }
            eventType = xpp.next();
        }
        return feed;
    }

    private void sendMessage(Feed feed) {
        Message message = this.handler.obtainMessage();
        message.what = 0;
        message.obj = feed;
        handler.sendMessage(message);
    }
}
