package firubingm.metropolia.users.rssfeedreader;


import java.text.SimpleDateFormat;
import java.util.Date;

public class FeedItem {

    private static final SimpleDateFormat format = new SimpleDateFormat("hh:mm dd.MM.yyyy");

    private String title;
    private String description;
    private String url;
    private Date publishDate;
    private boolean read;

    public FeedItem() {
        this.title = "";
        this.description = "";
        this.url = "";
        this.publishDate = null;
        this.read = false;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public void markAsRead() {
        this.read = true;
    }

    public void markAsUnread() {
        this.read = false;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString() {
        return "Feed Item: " + this.title + " - " + this.description;
    }

    public boolean getRead() {
        return this.read;
    }

    public String getDate() {
        if (this.publishDate != null) {
            return FeedItem.format.format(this.publishDate);
        } else {
            return "";
        }
    }
}
