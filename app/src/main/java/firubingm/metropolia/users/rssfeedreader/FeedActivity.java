package firubingm.metropolia.users.rssfeedreader;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;

public class FeedActivity extends Activity {

    private Feed feed;
    private FeedItemListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        ListView listView = (ListView)findViewById(R.id.listView);
        FeedStorage storage = FeedStorage.getInstance();
        Intent intent = getIntent();
        this.feed = storage.getFeed(intent.getIntExtra(MainActivity.OPEN_FEED, 0));
        this.adapter = new FeedItemListAdapter(this, R.layout.feed_item_layout, this.feed.getItems());
        listView.setAdapter(this.adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                handleClick(view, position);
            }
        });

        Switch hideSwitch = (Switch)findViewById(R.id.switch1);
        hideSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filterList();
            }
        });
    }

    private void handleClick(View view, int position) {
        CheckBox box = (CheckBox)view.findViewById(R.id.checkBox);
        box.performClick();
        this.adapter.handleClick(position, box.isChecked());
        this.filterList();
    }

    private void filterList() {
        Switch hideSwitch = (Switch)findViewById(R.id.switch1);
        if (hideSwitch.isChecked()) {
            adapter.getFilter().filter("filter");
        } else {
            adapter.getFilter().filter("show all");
        }
    }

}
