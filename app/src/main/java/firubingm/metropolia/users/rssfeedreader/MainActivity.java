package firubingm.metropolia.users.rssfeedreader;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


public class MainActivity extends Activity {

    public final static String OPEN_FEED = "firubingm.metropolia.users.rssfeedreader.OPEN_FEED";

    private static boolean previouslyStarted = false;

    private ListView listView;
    private FeedListAdapter adapter;
    private FeedStorage storage;
    private Button addButton;

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                Feed feed = (Feed) msg.obj;
                storage.addFeed(feed);
                adapter.add(feed);
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storage = FeedStorage.getInstance();
        listView = (ListView)findViewById(R.id.listView);
        adapter = new FeedListAdapter(this, R.layout.feed_layout, storage.getFeeds());
        listView.setAdapter(adapter);
        addButton = (Button)findViewById(R.id.addButton);

        if(!MainActivity.previouslyStarted) {
            MainActivity.previouslyStarted = true;
            String[] urls = {
                    "http://yle.fi/uutiset/rss/uutiset.rss?osasto=news",
                    "http://messi.hyyravintolat.fi/rss/eng/27",
                    "http://feeds.bbci.co.uk/news/rss.xml?edition=uk",
                    "http://www.cctv.com/program/rss/02/01/index.xml",
                    "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
            };
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                for (String url : urls) {
                    RssParser parser = new RssParser(this.handler, url);
                    parser.start();
                }
            }
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                handleClick(position);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked!");
                addFeed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handleClick(int position) {
        Intent intent = new Intent(this, FeedActivity.class);
        intent.putExtra(OPEN_FEED, position);
        startActivity(intent);
    }

    private void addFeed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        edittext.setTextColor(ContextCompat.getColor(this, R.color.primary_text_material_dark));
        alert.setMessage("Enter the URL for your feed");
        alert.setTitle("Add a feed");
        alert.setView(edittext);

        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String url = edittext.getText().toString();
                RssParser parser = new RssParser(handler, url);
                parser.start();
            }
        });

        alert.show();
    }
}
